**Install Raspberry Pi OS & configure your Raspberry Pi**: Your Pi is probably already running some software, but if you're starting from scratch, check out this [guide on setting up a new Raspberry Pi](https://howchoo.com/pi/how-to-set-up-a-new-raspberry-pi).

**How to Set Up a New Raspberry Pi**
Start using that brand new Pi.

**Shut down your Pi**:If your Pi is currently running, safely shut it down by connecting to it and running the following command:

**Connect the PCB (HAT)**: Carefully press the UPS onto your Pi's GPIO header. Then, use the included screws and plastic standoffs to secure it to your Pi. If you're using a Raspberry Pi Zero, you may need to solder a header onto the Pi first (unless you bought the "WH" version).

**Connect the battery, if not already connected**: Be sure to remove the battery's clear plastic insulation tab. If you purchased the PiJuice without a battery or want to add more power capacity with a separate Lithium-ion or Lithium-polymer battery, simply solder on a standard battery terminal and connect that battery instead.

**Connect a power source and power on the Pi**: Connect a normal Raspberry Pi power supply to the UPS power port.

**You're done**: If all you wanted to do was build a simple Raspberry Pi UPS, you're up and running already.
