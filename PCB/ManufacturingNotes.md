**Manufacturing Notes**

- Carefully select your PCB manufacturer. Ensure that research has been done to find nearby manufacturers, select a reliable and affordable manufacturer. It s recommended that you obtain multiple quotes before selecting a manufacturer.
- Send PCB file found [here ](https://gitlab.com/design-group-1/ups-pihat/-/blob/master/PCB/raspberrypi_zerow_uhat.kicad_pcb)
- Send Gerber files found [here](https://gitlab.com/design-group-1/ups-pihat/-/tree/master/PCB/Gerber%20Files)
- Send Centroid files which can be found here [top](https://gitlab.com/design-group-1/ups-pihat/-/blob/master/PCB/raspberrypi_zerow_uhat-top.pos) and [bottom](https://gitlab.com/design-group-1/ups-pihat/-/blob/master/PCB/raspberrypi_zerow_uhat-bottom.pos)
- Include Bill Of Materials when sending PCB file to supplier which can be found [here](https://gitlab.com/design-group-1/ups-pihat/-/blob/master/PCB/BOM.csv )

