**ON AND OFF Status LED (Single LED D6)**
- If the LED is ON, the battery is supplying the RasberryPi and the UPS is ON.
- If the LED is OFF, the battery is not supplying the RasberryPi and the UPS is OFF. 

**Battery Percentage Status LED  (Three pair LEDs)**
- If all three LEDs are ON(D7 - RED, D8 - Orange and D9 - Green), the battery is fully charged.
- If two LEDs are on(D7 and D8), the battery is at medium potential.
- If single LED(D7) is ON, battery is low and needs to be charged, the UPS will not have enough power to operated RasberryPi.
