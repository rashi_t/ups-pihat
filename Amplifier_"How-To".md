"How-To":
When connecting an input to the opamp it can either be
connected to the noninverting or the inverting input.
This depends on the purpose of the circuit.
For an inverter signal, connect the input to the inverting
input and vice versa for a noninverting signal.
OpAmps works well for amplification of voltages. 
The output of the opamp will be an amplified signal of 
the input signal. 
The waveform of the signal outputted entirely depends on
the waveform of the input signal.
