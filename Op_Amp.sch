EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:Opamp_Dual_Generic U1
U 1 1 60B92054
P 5250 3400
F 0 "U1" H 5250 3033 50  0000 C CNN
F 1 "Opamp_Dual_Generic" H 5250 3124 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SuperSOT-3" H 5250 3400 50  0001 C CNN
F 3 "~" H 5250 3400 50  0001 C CNN
	1    5250 3400
	1    0    0    1   
$EndComp
Wire Wire Line
	5550 3400 5850 3400
Text GLabel 6300 3400 2    50   Input ~ 0
Vout_Op_Amp
Wire Wire Line
	5850 3400 5850 2800
Wire Wire Line
	5850 2800 5400 2800
Connection ~ 5850 3400
Wire Wire Line
	5850 3400 6300 3400
$Comp
L Device:R_US R5
U 1 1 60B9269B
P 5250 2800
F 0 "R5" V 5045 2800 50  0000 C CNN
F 1 "20" V 5136 2800 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 5290 2790 50  0001 C CNN
F 3 "~" H 5250 2800 50  0001 C CNN
	1    5250 2800
	0    1    1    0   
$EndComp
Wire Wire Line
	5100 2800 4800 2800
Wire Wire Line
	4950 3300 4800 3300
Wire Wire Line
	4800 2800 4800 3300
Connection ~ 4800 3300
Wire Wire Line
	4800 3300 4650 3300
$Comp
L Device:R_US R4
U 1 1 60B934D3
P 4500 3300
F 0 "R4" V 4295 3300 50  0000 C CNN
F 1 "20" V 4386 3300 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 4540 3290 50  0001 C CNN
F 3 "~" H 4500 3300 50  0001 C CNN
	1    4500 3300
	0    1    1    0   
$EndComp
Wire Wire Line
	4950 3500 4800 3500
$Comp
L Device:D_Zener D10
U 1 1 60B93EDF
P 4450 3950
F 0 "D10" H 4450 3733 50  0000 C CNN
F 1 "0.85V" H 4450 3824 50  0000 C CNN
F 2 "Diode_THT:D_5KPW_P7.62mm_Vertical_AnodeUp" H 4450 3950 50  0001 C CNN
F 3 "~" H 4450 3950 50  0001 C CNN
	1    4450 3950
	-1   0    0    1   
$EndComp
Wire Wire Line
	4600 3950 4800 3950
Wire Wire Line
	4800 3500 4800 3950
Wire Wire Line
	4300 3950 4100 3950
Text GLabel 4100 3950 0    50   Input ~ 0
Ground
Wire Wire Line
	4800 3950 5200 3950
Connection ~ 4800 3950
$Comp
L Device:R_US R10
U 1 1 60B95383
P 5350 3950
F 0 "R10" V 5145 3950 50  0000 C CNN
F 1 "12000" V 5236 3950 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 5390 3940 50  0001 C CNN
F 3 "~" H 5350 3950 50  0001 C CNN
	1    5350 3950
	0    1    1    0   
$EndComp
Wire Wire Line
	5500 3950 5800 3950
Text GLabel 5800 3950 2    50   Input ~ 0
Vout_Battery
$Comp
L Analog_DAC:DAC081C081CIMK U2
U 1 1 60BA2F49
P 2300 3300
F 0 "U2" H 2744 3346 50  0000 L CNN
F 1 "DAC081C081CIMK" H 2744 3255 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6" H 2950 3050 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/dac081c081.pdf" H 2300 3300 50  0001 C CNN
	1    2300 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 3300 4350 3300
Text GLabel 2300 3600 0    50   Input ~ 0
Ground
Text HLabel 1250 3200 0    50   Input ~ 0
R1_SCL
Text HLabel 1250 3350 0    50   Input ~ 0
R1_SDA
Wire Wire Line
	1250 3200 1900 3200
Wire Wire Line
	1250 3350 1650 3350
Wire Wire Line
	1650 3350 1650 3300
Wire Wire Line
	1650 3300 1900 3300
NoConn ~ 1900 3400
$Comp
L power:+5V #PWR0107
U 1 1 60BA8166
P 2300 3000
F 0 "#PWR0107" H 2300 2850 50  0001 C CNN
F 1 "+5V" H 2315 3173 50  0000 C CNN
F 2 "" H 2300 3000 50  0001 C CNN
F 3 "" H 2300 3000 50  0001 C CNN
	1    2300 3000
	1    0    0    -1  
$EndComp
$EndSCHEMATC
